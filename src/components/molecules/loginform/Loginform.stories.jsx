import { userEvent, within } from "@storybook/testing-library";
import Loginform from "./Loginform";
import { expect } from "@storybook/jest";

export default {
  title: "Molecules/LoginForm",
  tags: ["autodocs"],
  component: Loginform,
};
export const emptyForm = {};

export const onLogin = {
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    await userEvent.type(
      canvas.getByPlaceholderText("Enter email"),
      "Tuantvt000$@gmail.com"
    );
    await userEvent.type(
      canvas.getByPlaceholderText("Enter password"),
      "123456789"
    );
    await userEvent.click(canvas.getByRole("button"));
    await expect(canvas.getByText("Login successfully")).toBeInTheDocument();
  },
};
