import React, { memo, useState } from "react";
import PropTypes from "prop-types";
import { Banner, Button, Input } from "../../atomic";
import User from "../../../data/user.json";

function Loginform() {
  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const [alert, setAlert] = useState({ variant: "", text: "" });
  const handleLogin = (e) => {
    e.preventDefault();
    const { email, password } = user;
    const useracc = User.find((useracc) => useracc.email === email);
    if (useracc) {
      if (useracc.password !== password) {
        setUser((prevState) => ({
          ...prevState,
          email: "",
          password: "",
        }));
        setAlert({ variant: "danger", text: "Login fail" });
        return;
      } else {
        setUser((prevState) => ({
          ...prevState,
          email: "",
          password: "",
        }));
        console.log(user.email, user.password);
        setAlert({ variant: "congrats", text: "Login successfully" });
      }
    } else {
      setUser((prevState) => ({
        ...prevState,
        email: "",
        password: "",
      }));
      setAlert({ variant: "danger", text: "Login fail" });
      return;
    }
  };
  return (
    <form>
      <Input
        placeText="Enter email"
        name="email"
        type="email"
        onchange={(e) => setUser({ ...user, email: e.target.value })}
        email={user.email}
      />
      <Input
        placeText="Enter password"
        name="password"
        type="password"
        onchange={(e) => setUser({ ...user, password: e.target.value })}
        password={user.password}
      />
      {alert.variant && (
        <Banner variant={alert.variant} children={alert.text} />
      )}
      <Button
        size="medium"
        text="Signin"
        onClick={handleLogin}
        status="success"
      />
    </form>
  );
}

export default memo(Loginform);
