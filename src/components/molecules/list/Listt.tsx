import { Avatar, List } from "antd";
import React, { memo } from "react";

type ListItem = {
  title: string;
  avatar?: string;
  description?: string;
};

type ListProps = {
  data: ListItem[];
  size: "default" | "large" | "small";
  itemLayout: "horizontal" | "vertical";
};

const Listt: React.FC<ListProps> = ({ data, size, itemLayout }) => (
  <List
    size={size}
    itemLayout={itemLayout}
    dataSource={data}
    renderItem={(item, index) => (
      <List.Item>
        <List.Item.Meta
          avatar={<Avatar src={item.avatar} />}
          title={<a href="https://ant.design">{item.title}</a>}
          description={item.description}
        />
      </List.Item>
    )}
  />
);

export default memo(Listt);
