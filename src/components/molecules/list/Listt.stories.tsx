import Listt from "./Listt";

export default {
  title: "molecules/List",
  component: Listt,
  tags: ["autodocs"],
  argTypes: {
    itemLayout: { control: "radio" },
    size: { control: "radio" },
  },
};

export const Default = {
  args: {
    size: "large",
    itemLayout: "horizontal",
    data: [
      {
        avatar: `https://xsgames.co/randomusers/avatar.php?g=pixel&key=1`,
        title: "Ant Design Title 1",
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team'
      },
      {
        avatar: `https://xsgames.co/randomusers/avatar.php?g=pixel&key=2`,
        title: "Ant Design Title 1",
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team'
      },
      {
        avatar: `https://xsgames.co/randomusers/avatar.php?g=pixel&key=3`,
        title: "Ant Design Title 1",
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team'
      },
      {
        avatar: `https://xsgames.co/randomusers/avatar.php?g=pixel&key=4`,
        title: "Ant Design Title 1",
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team'
      },
    ],
  },
};
