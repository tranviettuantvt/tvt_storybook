import Card from "./Card";
import { expect } from "@storybook/jest";
import { userEvent, within } from "@storybook/testing-library";

export default {
  title: "molecules/CardAvatar",
  component: Card,
  tags: ["autodocs"],
};

export const contactSuccess={}

export const contactTest={
    play: async ({ canvasElement }) => {
        const canvas = within(canvasElement);
        await userEvent.click(canvas.getByRole("button"));
        await expect(canvas.getByText("You are friends now")).toBeInTheDocument();
      },
}
