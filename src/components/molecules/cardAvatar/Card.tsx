import React, { memo, useState } from "react";
import styled from "@emotion/styled";
import { Typography } from "../../atomic/typography";
import { Button } from "../../atomic/button";
import { StyledAvatar } from "../card/Styles";

const StyledCard = styled.div`
  row-gap: 8px;
  width: 30%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const CardContent = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 16px;
  margin-bottom: 16px;
  text-align: center;
`;



const StyledHeading = styled(Typography)`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 8px;
`;

const StyledText = styled(Typography)`
  font-size: 16px;
  color: #555;
  margin-bottom: 16px;
`;

type Props = {
  image?: React.ReactNode;
  heading?: React.ReactNode;
  text?: React.ReactNode;
  button?: React.ReactNode;
};
function Card(props: Props) {
  const [status, setStatus] = useState({ variant: "", text: "" });

  const handleClick = (e: any) => {
    e.preventDefault();
    setStatus({ variant: "congrats", text: "You are friends now" });
  };

  return (
    <StyledCard>
      <StyledAvatar
        size="medium"
        shape="square"
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU"
      />
      <CardContent>
        <StyledHeading
          type="text"
          level="heading"
          content="Hello, I am Captain America from Brooklyn"
        />
        <StyledText
          type="text"
          level="text"
          content="Steve Rogers was a would-be U.S. Army enlistee rejected by recruiters because of his small size. He volunteers to receive a top-secret serum and transforms into a “super-soldier.”"
        />
        <Button
          size="medium"
          status="success"
          onClick={handleClick}
          content="Contact me"
        />
      </CardContent>
    </StyledCard>
  );
}

export default memo(Card);
