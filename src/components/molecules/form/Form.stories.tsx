import Form from "./Form";

export default {
  title: "Molecules/Form",
  component: Form,
  tags: ["autodocs"],
};

export const formLogin = {
  args: {
    fields: [
      { placeText: "Enter Name", type: "text", name: "name" },
      { placeText: "Enter Email", type: "email", name: "email" },
      { placeText: "Enter Password", type: "password", name: "password" },
    ],
    buttons: [{ content: "Submit" }, { content: "Reset" }],
  },
};

export const formInfor ={
    args:{
        fields: [
            { placeText: "Enter Name", type: "text", name: "name" },
            { placeText: "Enter Address", type: "email", name: "email" },
            { placeText: "Enter Phone", type: "number", name: "number" },
    
          ],
          buttons: [{ content: "Contact" }],
    }
   
}