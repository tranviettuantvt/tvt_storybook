import React, { memo } from "react";
import { Input } from "../../atomic/input";
import { StyledButtonForm, StyledForm } from "./styles";
import { Button } from "../../atomic/button";

type Field = {
  type: string;
  name: string;
  placeText: string;
};

type Button = {
  content: string;
};

type FormProps = {
  fields: Field[];
  buttons?: Button[];
};

function Form(props: FormProps) {
  const { fields, buttons } = props;
  return (
    <StyledForm>
      {fields.map((field) => (
        <Input
          placeText={field.placeText}
          key={field.name}
          type={field.type}
          name={field.name}
        />
      ))}
      <StyledButtonForm>
        {buttons &&
          buttons.map((button) => <Button content={button.content} />)}
      </StyledButtonForm>
    </StyledForm>
  );
}

export default memo(Form);
