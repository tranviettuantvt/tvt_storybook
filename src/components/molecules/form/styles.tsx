import styled from "@emotion/styled";
export const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const StyledButtonForm=styled.div`
    display: flex;
    justify-content: start;
    gap: 18px;
`