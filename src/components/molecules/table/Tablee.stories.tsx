import Tablee from "./Tablee";

export default {
  title: "molecules/Table",
  component: Tablee,
  tags: ["autodocs"],
};


export const Default = {
  args: {
    dataSource: [
        { key: '1', name: 'John Doe', age: 30, address: '123 Street' },
        { key: '2', name: 'Jane Smith', age: 28, address: '456 Avenue' },
        { key: '3', name: 'Bob Johnson', age: 35, address: '789 Road' },
      ],
      columns: [
        { title: 'Name', dataIndex: 'name', key: 'name' },
        { title: 'Age', dataIndex: 'age', key: 'age' },
        { title: 'Address', dataIndex: 'address', key: 'address' },
      ],
      pagination: { pageSize: 10 },
      loading: false,
      bordered: true,
      size: 'middle',
      rowKey: 'key',
      rowSelection: {
        type: 'checkbox',
        onChange: (selectedRowKeys: any, selectedRows: any) => {
          console.log('Selected Row Keys:', selectedRowKeys);
          console.log('Selected Rows:', selectedRows);
        },
      },
      onRow: (record: any) => {
        return {
          onClick: () => {
            console.log('Clicked Row:', record);
          },
        };
      },
      onChange: (pagination: any, filters: any, sorter: any) => {
        console.log('Pagination:', pagination);
        console.log('Filters:', filters);
        console.log('Sorter:', sorter);
      },
  },
};
