import React, { memo } from 'react';
import { Table } from 'antd';

interface TableProps {
  dataSource: Array<any>;
  columns: Array<any>;
  pagination?: any;
  loading?: boolean;
  bordered?: boolean;
  size?: 'large' | 'middle' | 'small';
  rowKey?: string | ((record: any) => string);
  rowSelection?: any;
  onRow?: (record: any, index?: number) => any;
  onChange?: (pagination: any, filters: any, sorter: any) => void;
}

const CustomTable: React.FC<TableProps> = ({
  dataSource,
  columns,
  pagination,
  loading,
  bordered,
  size='middle',
  rowKey,
  rowSelection,
  onRow,
  onChange,
}) => {
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={pagination}
      loading={loading}
      bordered={bordered}
      size={size}
      rowKey={rowKey}
      rowSelection={rowSelection}
      onRow={onRow}
      onChange={onChange}
    />
  );
};

export default memo(CustomTable);
