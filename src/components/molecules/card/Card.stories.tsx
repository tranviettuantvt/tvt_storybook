import Card from "./Card";

export default {
  title: "Molecules/Card",
  component: Card,
  tags: ["autodocs"],
};
export const Custom= {
  args :{
    src:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU",
    title:"tuan dep trai",
    content:"tuan sieu cap dep trai",
    children: <h2>Tuan binh thuong</h2>
  }
}
export const Default = () => (
  <Card
    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU"
    title="tuan dep trai"
    content="tuan sieu cap dep trai"
  />
);


