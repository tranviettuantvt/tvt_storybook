import styled from "@emotion/styled";
import { Typography } from "../../atomic/typography";
import { Avatar } from "../../atomic/avatar";

export const StyledCard = styled.div`
  row-gap: 8px;
  width: 20%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

export const StyledAvatar = styled(Avatar)`
  margin-bottom: 16px;
`;

export const CardContent = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 16px;
  text-align: center;
`;

export const StyledHeading = styled(Typography)`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const StyledText = styled(Typography)`
  font-size: 16px;
  color: #555;
  margin-bottom: 16px;
`;