import React, { memo } from "react";
import {
  CardContent,
  StyledAvatar,
  StyledCard,
  StyledHeading,
  StyledText,
} from "./Styles";
import PropTypes from "prop-types";

type CardProps = {
  children?: React.ReactNode;
  src?: string;
  title?: string;
  content?: string;
};

function Card(props: CardProps) {
  const { src, title, content, children } = props;
  return (
    <StyledCard>
      {src && <StyledAvatar size="medium" shape="square" src={src} />}
      <CardContent>
        {title && <StyledHeading type="text" level="heading" content={title} />}
        {content && <StyledText type="text" level="text" content={content} />}
        {children}
      </CardContent>
    </StyledCard>
  );
}

export default memo(Card);

Card.propTypes = {
  src: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  children: PropTypes.node,
};

Card.defaultProps = {
  children: "",
  src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU",
  title: "I am captain",
  content: "I am captain america from brookLyn",
};
