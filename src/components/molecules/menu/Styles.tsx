import { Dropdown } from "../../atomic/dropdown";
import styled from "@emotion/styled";

export const StyledMenu = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 16px;
  border-bottom: 1px solid #dbdbdb;
`;

export const StyleDMenuDrop = styled(Dropdown)`
  background: none;
`;