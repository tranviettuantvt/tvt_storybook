import Menu from "./Menu";

export default {
  title: "molecules/Menu",
  component: Menu,
  tags: ["autodocs"],
};

export const Default = {
  args: {
    items: [
      {
        key: "1",
        menu_item: "Marvel",
        icon: "⍟",
        children: [
          {
            key: "1",
            label: "Captain America",
          },
          {
            key: "2",
            label: "Iron Man",
          },
        ],
      },
      {
        key: "2",
        menu_item: "DC",
        icon: "⍟",
        children: [
          {
            key: "1",
            label: "SuperMan",
          },
          {
            key: "2",
            label: "BatMan",
          },
        ],
      },
      {
        key: "3",
        menu_item: "VNPT",
        icon: "⍟",
      },
      {
        key: "3",
        menu_item: "Viettel",
        icon: "⍟",
        children: [
          {
            key: "1",
            label: "Viettel Monney",
          },
          {
            key: "2",
            label: "Viettel Pay",
          },
        ],
      },
    ],
  },
};
