import React, { memo } from "react";
import { StyleDMenuDrop, StyledMenu } from "./Styles";


type DropdownItem = {
  key: string;
  label: React.ReactNode;
};

type MenuItem = {
  key: string;
  menu_item: string;
  icon?: string;
  children?: DropdownItem[];
};

type MenuProps = {
  items: MenuItem[];
};
function Menu({ items }: MenuProps) {
  return (
    <StyledMenu>
      {items.map((item) => (
        <>
          <StyleDMenuDrop
            icon={item.icon}
            text={item.menu_item}
            items={item.children}
            action="hover"
          />
        </>
      ))}
    </StyledMenu>
  );
}

export default memo(Menu);
