import { Form } from "../form";
import Modall from "./Modall";

export default {
  title: "molecules/Modal",
  component: Modall,
  tags: ["autodocs"],
};

export const Default = {
  args: {
    children: (
        <Form
          fields={[
            {
              name: "name",
              placeText: "Enter Name",
              type: "text",
            },
            {
              name: "email",
              placeText: "Enter Email",
              type: "email",
            },
            {
              name: "password",
              placeText: "Enter Password",
              type: "password",
            },
          ]}
        />
    ),
  },
};
