export const SUCCESS_COLOR='#4caf50'
export const ERROR_COLOR='#f44336'
export const WARNING_COLOR='#f57c00'
export const DOCUMENTATION_COLOR='#44a9ba'
export const TEXT_COLOR='#919191'
export const BACKGROUND_HOVER='#f3f1f1'