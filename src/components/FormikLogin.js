import {
  Alert,
  Box,
  Button,
  Container,
  TextField,
  Typography,
} from "@mui/material";
import { ErrorMessage, Formik } from "formik";
import React from "react";
import * as Yup from "yup";
import User from "../data/user.json";
import { useNavigate } from "react-router-dom"; // Import the useNavigate hook from react-router-dom

function FormikLogin() {
  const navigate = useNavigate(); // Initialize the useNavigate hook

  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      onSubmit={(values, { setSubmitting, setFieldError }) => {
        setTimeout(() => {
          const matchedUser = User.find(
            (user) =>
              user.email === values.email && user.password === values.password
          );
          if (matchedUser) {
            const { email, password } = matchedUser;
            setSubmitting(false);
            const userData = { email, password, token: "1" };
            localStorage.setItem("userData", JSON.stringify(userData));
            navigate("/");
          } else {
            // Login failed, display an error
            setSubmitting(false);
            setFieldError("password", "Invalid email or password");
          }
        }, 500);
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email().required("Required"),
        password: Yup.string()
          .required("No password provided.")
          .min(8, "Password is too short - should be 8 chars minimum.")
          .matches(/(?=.*[0-9])/, "Password must contain a number."),
      })}
    >
      {(props) => {
        const {
          handleSubmit,
          isSubmitting,
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
        } = props;

        return (
          <Container component="main">
            <Box
              sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Box
                component="form"
                sx={{ mt: 1, width: "532px" }}
                onSubmit={handleSubmit}
              >
                <TextField
                  fullWidth
                  margin="normal"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {errors.email && touched.email && (
                  <Alert severity="error">{errors.email}</Alert>
                )}
                <TextField
                  fullWidth
                  margin="normal"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {errors.password && touched.password && (
                  <Alert severity="error">{errors.password}</Alert>
                )}
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  disabled={isSubmitting}
                >
                  Sign In
                </Button>
                {/* {submit && <Alert severity="success">Success Login</Alert>} */}
              </Box>
            </Box>
          </Container>
        );
      }}
    </Formik>
  );
}

export default FormikLogin;
