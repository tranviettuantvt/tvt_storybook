import Pagination from "./Pagination";

export default {
  title: "atomic/Pagination",
  component: Pagination,
  tags: ["autodocs"],
};

export const Default = {
    args: {
        currentPage: 1,
        totalPages: 5,
        onPageChange: (page:number) => console.log("Page changed to", page),
    }
}