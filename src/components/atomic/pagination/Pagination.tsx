import React, { memo, useState, useEffect } from "react";
import styled from "@emotion/styled";
import { SUCCESS_COLOR } from "../../constants";

type PaginationProps = {
  currentPage: number;
  totalPages: number;
  onPageChange: (page: number) => void;
};

const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

const ArrowButton = styled.span<{ isDisabled: boolean }>`
  margin: 0 5px;
  padding: 5px 10px;
  border: none;
  background-color: transparent;
  color: ${(props) => (props.isDisabled ? "#ccc" : "#000")};
  cursor: ${(props) => (props.isDisabled ? "default" : "pointer")};

  &:hover {
    color: ${(props) => (props.isDisabled ? "#ccc" : "#555")};
  }
`;
const PageButton = styled.button<{ isActive: boolean }>`
  margin: 0 5px;
  padding: 5px 10px;
  border: none;
  background-color: ${(props) =>
    props.isActive ? SUCCESS_COLOR : "transparent"};
  cursor: pointer;

  ${(props) =>
    props.isActive &&
    `
    color: white;
    border-radius: 5px;
  `}
`;

function Pagination({
  currentPage,
  totalPages,
  onPageChange,
}: PaginationProps) {
  const arrowLeft = "<";
  const arrowRight = ">";
  const [activePage, setActivePage] = useState(currentPage);
  const handlePageChange = (page: number) => {
    if (page >= 1 && page <= totalPages && page !== activePage) {
      setActivePage(page);
      onPageChange(page);
    }
  };

  const handlePrevPage = () => {
    let prevPage;
    if (activePage === 1) {
      prevPage = totalPages;
    } else {
      prevPage = activePage - 1;
    }
    setActivePage(prevPage);
    onPageChange(prevPage);
  };

  const handleNextPage = () => {
    let nextPage;
    if (activePage === totalPages) {
      nextPage = 1;
    } else {
      nextPage = activePage + 1;
    }
    setActivePage(nextPage);
    onPageChange(nextPage);
  };

  useEffect(() => {
    setActivePage(currentPage);
  }, [currentPage]);
  return (
    <PaginationContainer>
      <ArrowButton isDisabled={currentPage === 1} onClick={handlePrevPage}>
        {arrowLeft}
      </ArrowButton>
      {Array.from({ length: totalPages }, (_, index) => index + 1).map(
        (page) => (
          <PageButton
            key={page}
            isActive={page === activePage}
            onClick={() => handlePageChange(page)}
          >
            {page}
          </PageButton>
        )
      )}
      <ArrowButton
        isDisabled={currentPage === totalPages}
        onClick={handleNextPage}
      >
        {arrowRight}
      </ArrowButton>
    </PaginationContainer>
  );
}

export default memo(Pagination);
