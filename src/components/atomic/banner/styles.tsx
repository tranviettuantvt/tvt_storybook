import styled from "@emotion/styled";
import { DOCUMENTATION_COLOR, ERROR_COLOR, SUCCESS_COLOR } from "../../constants";

export const BeforeIcon = styled.span`
  width: 35px;
  height: 35px;
  display: inline-flex;
  position: absolute;
  left: -1.2rem;
  border-radius: 50%;
  align-items: center;
`;

export const InfoBanner = styled.aside`
  margin: 1.5rem 0;
  border-radius: 0px 10px 10px 0px;
  padding: 0.8em 1em;
  line-height: 1.2;
  position: relative;
  clear: both;
  max-width: 500px;
  border-left: 4px solid #b4aaff;
  background-color: rgba(224, 226, 255, 0.5);
  color: #2a2135;
`;

export const DangerBanner = styled(InfoBanner)`
  border-left: 4px solid ${ERROR_COLOR};
  background-color: rgb(253, 236, 234);
`;

export const CongratsBanner = styled(InfoBanner)`
  border-left: 4px solid ${SUCCESS_COLOR};
  background-color: rgb(249, 253, 234);
`;

export const DocumentationBanner = styled(InfoBanner)`
  border-left: 4px solid ${DOCUMENTATION_COLOR};
  background-color: rgb(234, 248, 253);
`;