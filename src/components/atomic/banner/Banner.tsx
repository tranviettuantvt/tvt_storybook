import React, { memo, ReactNode } from "react";
import PropTypes from "prop-types";
import {
  BeforeIcon,
  CongratsBanner,
  DangerBanner,
  DocumentationBanner,
  InfoBanner,
} from "./styles";

type BannerProps = {
  variant: "info" | "congrats" | "documentation" | "danger";
  children: ReactNode;
};

function Banner({ variant, children }: BannerProps) {
  let BannerComponent;

  switch (variant) {
    case "info":
      BannerComponent = InfoBanner;
      break;
    case "danger":
      BannerComponent = DangerBanner;
      break;
    case "congrats":
      BannerComponent = CongratsBanner;
      break;
    case "documentation":
      BannerComponent = DocumentationBanner;
      break;
    default:
      BannerComponent = InfoBanner;
      break;
  }

  return (
    <BannerComponent>
      <BeforeIcon>{getIconContent(variant)}</BeforeIcon>
      {children}
    </BannerComponent>
  );
}

function getIconContent(variant: string) {
  switch (variant) {
    case "info":
      return "🔑";
    case "danger":
      return "⚠️";
    case "congrats":
      return "🎉";
    case "documentation":
      return "📚";
    default:
      return "";
  }
}

export default memo(Banner);

Banner.propTypes = {
  variant: PropTypes.oneOf(["info", "congrats", "documentation", "danger"]),
};
