import React from "react";
import Banner from "./Banner";

export default {
  title: "Atomic/Banner",
  component: Banner,
  argTypes: {
    variant: {
      control: {
        type: "radio",
        options: ["info", "congrats", "documentation", "danger"],
      },
    },
  },
};

type BannerProps = {
  variant: "info" | "congrats" | "documentation" | "danger";
  children: React.ReactNode;
};

const Template = (args: BannerProps) => <Banner {...args} />;

export const Info = Template.bind({});
Info.args = {
  variant: "info",
  children:
    "This is an example of an info banner to display important information.",
};

export const Danger = Template.bind({});
Danger.args = {
  variant: "danger",
  children:
    "This is an example of a danger banner to display important information.",
};

export const Congrats = Template.bind({});
Congrats.args = {
  variant: "congrats",
  children:
    "This is an example of a congratulation banner to display important information.",
};

export const Documentation = Template.bind({});
Documentation.args = {
  variant: "documentation",
  children:
    "This is an example of a documentation banner to display important information.",
};