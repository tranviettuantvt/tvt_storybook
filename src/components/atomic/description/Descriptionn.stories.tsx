import Descriptionn from "./Descriptionn";

export default {
  title: "atomic/Description",
  component: Descriptionn,
  tags: ["autodocs"],
  argTypes: {
    border: { control: "boolean" },
    layout: { control: "radio" },
    size: { control: "radio" },
  },
};

export const Default = {
  args: {
    size: "large",
    title: "userInfor",
    items: [
      { label: "Product", text: "Cloud Database" },
      { label: "Billing Mode", text: "Prepaid" },
      { label: "Automatic Renewal", text: "YES" },
      { label: "Order time", text: "2018-04-24 18:00:00" },
      { label: "Usage Time", text: "2019-04-24 18:00:00" },
      {
        label: "Remark",
        text: "No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China",
      },
    ],
  },
};
