import React, { memo } from "react";
import { Descriptions } from "antd";

type DescriptionItem = {
  label: string;
  text: string;
};
type DescriptionnProps = {
  title?: string;
  layout?: "horizontal" | "vertical";
  size?: "default" | "middle" | "small";
  border?: boolean;
  items: DescriptionItem[];
};
const Descriptionn: React.FC<DescriptionnProps> = ({
  title,
  layout = "horizontal",
  size = "default",
  border = true,
  items,
}) => (
  <Descriptions title={title} bordered={border} size={size} layout={layout}>
    {items.map((item) => (
      <Descriptions.Item label={item.label}>{item.text}</Descriptions.Item>
    ))}
  </Descriptions>
);

export default memo(Descriptionn);
