import React, { memo, useState } from "react";
import { CollapseButton, CollapseContainer, CollapseContent } from "./styles";

type CollapseProps = {
  label: string;
  children: React.ReactNode;
};

const Collapse: React.FC<CollapseProps> = ({ label, children }) => {
  const [open, setOpen] = useState(false);
  const toggle = () => {
    setOpen(!open);
  };

  return (
    <CollapseContainer>
      <CollapseButton onClick={toggle}>{label}</CollapseButton>
      <CollapseContent open={open}>{children}</CollapseContent>
      <hr />
    </CollapseContainer>
  );
};

export default memo(Collapse);
