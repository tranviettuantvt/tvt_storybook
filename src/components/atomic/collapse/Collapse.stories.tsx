import Collapse from "./Collapse";

export default {
  title: "atomic/Collapse",
  component: Collapse,
  tags: ["autodocs"],
};

export const Default = () => (
  <>
    <Collapse label="Introduction">
      <h1>introduction</h1>
      <p>
        The Collapse component puts long sections of the information under a
        block enabling users to expand or collapse to access its details.
      </p>
    </Collapse>
    <Collapse label="Prerequisite">
      <h1>Prerequisite</h1>
      <p>
        I am going to assume that you know the basics like how a component
        works, how useState works, and how to set up React. I will only focus on
        building the project and though this is for an absolute beginner who
        would like to know how useEffect and useRef work.
      </p>
    </Collapse>
    <Collapse label="Goals ">
      <h1>Goals</h1>
      <p>
        This article will teach you more about the useState hook, useRef hook,
        and animation in React; we will make simple React Collapse components,
        useful for all kinds of apps. This Collapse element can be shown or
        hidden by clicking a button.
      </p>
    </Collapse>
  </>
);

export const Custom = {
  args: {
    label: "Captain America",
    children: (
      <>
        <h1>Captain America</h1>
        <p>
          A symbol to the nation, a hero to the world. the story of captain
          america is one of honor and bravery and sacrifice.A symbol to the
          nation, a hero to the world. the story of captain america is one of
          honor and bravery and sacrifice.
        </p>
      </>
    ),
  },
};
