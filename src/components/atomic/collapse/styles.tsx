import styled from "@emotion/styled";
import { css } from "@emotion/react";

export const CollapseContainer = styled.div`
  margin-bottom: 10px;
`;

export const CollapseButton = styled.button`
  background-color: #f0f0f0;
  border-radius: 4px;
  border: none;
  padding: 10px;
  font-weight: bold;
  cursor: pointer;
  transition: background-color 0s ease-in-out;

  &:hover {
    background-color: #e0e0e0;
  }
`;

export const CollapseContent = styled.div<{ open: boolean }>`
  overflow: hidden;
  transition: max-height 0.3s cubic-bezier(0.4, 0, 0.2, 1);
  max-height: 0;

  ${(props) =>
    props.open &&
    css`
      max-height: 500px; /* Set an appropriate value for the maximum height */
      transition: max-height 0.3s cubic-bezier(0.4, 0, 0.2, 1);
    `}
`;
