import React, { memo, MouseEventHandler } from "react";
import PropTypes from "prop-types";
import { StyledButton } from "./styles";

type ButtonProps= {
  size?: 'small' | 'medium' | 'large';
  content: string;
  status?: 'success' | 'error' | 'warning';
  backgroundColor?: string,
  onClick?: MouseEventHandler 
}


function Button(props: ButtonProps) {
  const { size, content, backgroundColor, onClick, status }=props
  return (
    <StyledButton
      className={`button--${size} button-${status}`}
      type="button"
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {content}
    </StyledButton>
  );
}

export default memo(Button);
Button.propTypes = {
  content: PropTypes.string.isRequired,

  backgroundColor: PropTypes.string,

  size: PropTypes.oneOf(["small", "medium", "large"]),
  status: PropTypes.oneOf(["success", "error", "warning"]),

  onClick: PropTypes.func,
};
Button.defaultProps = {
  status: "success",
  backgroundColor: null,
  size: "medium",
  onClick: undefined,
};
