import styled from "@emotion/styled";
import { ERROR_COLOR, SUCCESS_COLOR, WARNING_COLOR } from "../../constants";

export const StyledButton = styled.button`
  display: inline-block;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  font-size: 16px;
  cursor: pointer;
  transition: background-color 0.3s ease-out, color 0.3s ease-out;

  &.button--small {
    font-size: 14px;
    padding: 8px 16px;
  }
  &.button-success {
    background-color: ${SUCCESS_COLOR};
    color: #fff;
  }

  &.button-success:hover {
    background-color: ${SUCCESS_COLOR};
  }

  &.button-error {
    background-color: ${ERROR_COLOR};
    color: #fff;
  }

  &.button-error:hover {
    background-color: ${ERROR_COLOR};
  }

  &.button-warning {
    background-color: ${WARNING_COLOR};
    color: #fff;
  }

  &.button-warning:hover {
    background-color: ${WARNING_COLOR};
  }

  &.button--medium {
    font-size: 16px;
    padding: 12px 20px;
  }

  &.button--large {
    font-size: 18px;
    padding: 16px 24px;
  }

  &:hover {
    cursor: pointer;
  }
`;