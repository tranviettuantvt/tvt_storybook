import Button from "./Button";

export default {
  title: "Atomic/Button",
  component: Button,
  tags: ["autodocs"],
  argTypes: {
    backgroundColor: { control: "color" },
    onClick: { action: "clicked" },
  },
};

export const Search = {
  args: {
    content: "Search",
    size: "large",
    status:'success'
  },
};

export const Login = {
  args: {
    content: "Login",
    size: "medium",
    status: 'warning'
  },
};
