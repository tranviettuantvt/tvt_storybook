import React, { memo } from "react";
import PropTypes from "prop-types";
import { PhaseContainer, PhaseMissionItem, PhaseMissionList, PhaseTitle, StepContainer } from "./Styles";

type Mission = {
  key: string;
  mis: string;
};

type StepPhase = {
  title: string;
  missions?: Mission[];
};

type StepProps = {
  current: 0 | 1 | 2 | 3;
  items: StepPhase[];
};



function Step({ current, items }: StepProps) {
  return (
    <StepContainer>
      {items.map((phase, index) => {
        const opacity = index < current ? 1 : 0.5;

        return (
          <PhaseContainer key={index} opacity={opacity}>
            <PhaseTitle>{phase.title}</PhaseTitle>
            <PhaseMissionList>
              {phase.missions && phase.missions.map((mission) => (
                <PhaseMissionItem key={mission.key}>
                  {mission.mis}
                </PhaseMissionItem>
              ))}
            </PhaseMissionList>
          </PhaseContainer>
        );
      })}
    </StepContainer>
  );
}

export default memo(Step);

Step.propTypes = {
  current: PropTypes.oneOf([0, 1, 2, 3]),
};
