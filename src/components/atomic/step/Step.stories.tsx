import Step from "./Step";

export default {
  title: "Atomic/Step",
  component: Step,
  tags: ["autodocs"],
  argTypes: {
    current: { control: "radio" },
  },
};

export const Default = {
  args: {
    current: 0,
    items: [
      {
        title: "Finished",
        missions: [
          {
            key: "1",
            mis: "Work from home",
          },
          {
            key: "2",
            mis: "Study from home",
          },
        ],
      },
      {
        title: "In Progress",
        missions: [
          {
            key: "1",
            mis: "Play video game",
          },
          {
            key: "2",
            mis: "Run 1 mile and do burbee",
          },
        ],
      },
      {
        title: "Waiting",
        missions: [ {
            key: "1",
            mis: "Play everyday",
          },
          {
            key: "2",
            mis: "Sleep everyday",
          },],
      },
    ],
  },
};
