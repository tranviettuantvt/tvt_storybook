import styled from "@emotion/styled";

export const StepContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const PhaseContainer = styled.div<{ opacity: number }>`
  opacity: ${(props) => props.opacity};
  transition: opacity 0.3s ease-in-out;
  position: relative;

  &:not(:last-child)::after {
    opacity: ${(props) => props.opacity};
    transition: opacity 0.3s ease-in-out;
    display: block;
    content: "";
    position: absolute;
    left: 90%;
    width: 13rem;
    height: 1px;
    background-color: black;
    top: 32px;
  }
`;

export const PhaseTitle = styled.h3`
  font-size: 20px;
  margin-bottom: 8px;
`;

export const PhaseMissionList = styled.ul`
  padding: 0;
`;

export const PhaseMissionItem = styled.li`
  margin-bottom: 6px;
  font-size: 16px;
`;