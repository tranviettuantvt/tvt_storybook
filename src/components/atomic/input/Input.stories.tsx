import Input from "./Input";

export default {
  title: "Atomic/Input",
  component: Input,
  tags: ["autodocs"],
  argTypes: {
    onchange: { action: "handleInputChange" },
  },
};

export const emailInput = {
  args: {
    type: "email",
    placeText: "Email Address",
    name: "email",
    email: "Tuan000$@gmail.com",
  },
};

export const passwordInput = {
  args: {
    type: "password",
    placeText: "Enter Password",
    name: "password",
    password: "123456789",
  },
};
