import styled from "@emotion/styled";
import { SUCCESS_COLOR } from "../../constants";

export const StyledInput = styled.input`
  display: block;
  width: 30%;
  padding: 10px;
  border: none;
  border-radius: 5px;
  background-color: #f2f2f2;
  color: #333;
  font-size: 16px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  margin-bottom: 1rem;
  &:focus {
    color: ${SUCCESS_COLOR};
    outline: none;
    border-color: ${SUCCESS_COLOR};
    box-shadow: 0 0 3px ${SUCCESS_COLOR};
  }
`;