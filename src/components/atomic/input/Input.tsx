import React, { memo, ChangeEventHandler } from "react";
import PropTypes from "prop-types";
import { StyledInput } from "./styles";

type InputProps = {
  placeText?: string;
  type?: string;
  name?: string;
  email?: string;
  password?: string;
  onchange?: ChangeEventHandler<HTMLInputElement>;
};

function Input(props: InputProps) {
  const { placeText, email, password, onchange, name, type } = props;
  return (
    <>
      <StyledInput
        className="input"
        placeholder={placeText}
        value={email || password || ""}
        onChange={onchange}
        name={name}
        type={type}
      />
    </>
  );
}

export default memo(Input);

Input.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  type: PropTypes.string,
  placeText: PropTypes.string,
  name: PropTypes.string,
  onchange: PropTypes.func,
};

Input.defaultProps = {
  email: null,
  password: null,
  name: null,
  placeText: null,
  type: null,
  onchange: PropTypes.func.isRequired,
};
