import TimeLine from "./TimeLine";

export default {
  title: "atomic/Timeline",
  component: TimeLine,
  tags: ["autodocs"],
  argTypes: {
    mode: { control: "select" },
  },
};

export const Default = {
  args: {
    pending: "loading",
    mode: "left",
    items: [
      {
        children: "Create a services site 2015-09-01",
      },
      {
        children: "Solve initial network problems 2015-09-01",
      },
      {
        children: "Technical testing 2015-09-01",
      },
      {
        children: "Network problems being solved 2015-09-01",
      },
    ],
  },
};

export const Custom = {
  args: {
    items: [
      {
        color: "green",
        children: "Create a services site 2015-09-01",
      },
      {
        color: "green",
        children: "Create a services site 2015-09-01",
      },
      {
        color: "red",
        children: (
          <>
            <p>Solve initial network problems 1</p>
            <p>Solve initial network problems 2</p>
            <p>Solve initial network problems 3 2015-09-01</p>
          </>
        ),
      },
      {
        children: (
          <>
            <p>Technical testing 1</p>
            <p>Technical testing 2</p>
            <p>Technical testing 3 2015-09-01</p>
          </>
        ),
      },
      {
        color: "gray",
        children: (
          <>
            <p>Technical testing 1</p>
            <p>Technical testing 2</p>
            <p>Technical testing 3 2015-09-01</p>
          </>
        ),
      },
      {
        color: "gray",
        children: (
          <>
            <p>Technical testing 1</p>
            <p>Technical testing 2</p>
            <p>Technical testing 3 2015-09-01</p>
          </>
        ),
      },
    ],
  },
};
