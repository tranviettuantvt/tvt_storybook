import React, { memo } from 'react';
import { Timeline } from 'antd';

type TimelineItem={
    color?: string;
    children: React.ReactNode;
}

type TimelineProps ={
    pending?: string;
    mode?:'left' | 'alternate' | 'right'
    items: TimelineItem[]
}
const TimeLine: React.FC<TimelineProps>=({mode='left',pending='loading',items}) => {
    return <Timeline mode={mode} pending={pending} items={items}/>
}

export default memo(TimeLine)