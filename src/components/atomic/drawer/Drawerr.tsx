import React, { memo, useState } from 'react';
import type { RadioChangeEvent } from 'antd';
import { Button, Drawer, Radio, Space } from 'antd';

type DrawerrProps={
    position?: 'top'|'right'|'bottom'|'left';
    children: React.ReactNode
}

const Drawerr: React.FC<DrawerrProps> = ({position='left', children}) => {
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState<DrawerrProps['position']>(position);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const onChange = (e: RadioChangeEvent) => {
    setPlacement(e.target.value);
  };

  return (
    <>
      <Space>
        <Radio.Group value={placement} onChange={onChange}>
          <Radio value="top">top</Radio>
          <Radio value="right">right</Radio>
          <Radio value="bottom">bottom</Radio>
          <Radio value="left">left</Radio>
        </Radio.Group>
        <Button type="primary" onClick={showDrawer}>
          Open
        </Button>
      </Space>
      <Drawer
        title="Basic Drawer"
        placement={placement}
        closable={false}
        onClose={onClose}
        open={open}
        key={placement}
      >
        {children}
      </Drawer>
    </>
  );
};

export default memo(Drawerr);