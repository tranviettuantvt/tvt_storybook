import Drawerr from "./Drawerr";

export default {
  title: "atomic/Drawer",
  component: Drawerr,
  tags: ["autodocs"],
  argTypes: {
    position: { control: "radio" },
  },
};

export const Default = {
  args: {
    position: "left",
    children: (
      <>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </>
    ),
  },
};
