import React, { memo, useEffect, useState, useRef } from "react";
import {
  StyleMenuItem,
  StyledCloseMenu,
  StyledInput,
  StyledLabel,
  StyledMenu,
} from "./styles";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons';


type SelectMenuItem = {
  value: number;
  text: string;
};

type MultiSelectProps = {
  disabled?: boolean;
  options: SelectMenuItem[];
};

function MultiSelect({ disabled=false, options }: MultiSelectProps) {
  const [selectValue, setSelectValue] = useState<SelectMenuItem[]>([]);
  const [isOpen, setIsOpen] = useState(false);
  const selectRef = useRef<HTMLInputElement>(null);

  const openSelectMenu = () => {
    setIsOpen(true);
  };

  const handleOutsideClick = (event: MouseEvent) => {
    if (!selectRef.current || !event.target) {
      return;
    }
    if (!selectRef.current.contains(event.target as Node)) {
      setIsOpen(false);
    }
  };

  const handleSelect = (option: SelectMenuItem, event: React.MouseEvent) => {
    event.stopPropagation(); // Prevent event propagation
    if (!selectValue.some((item) => item.value === option.value)) {
      setSelectValue((prev) => [...prev, option]);
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, []);

  return (
    <div>
      <StyledLabel onClick={openSelectMenu} htmlFor="multiselect">
        Please select your items
      </StyledLabel>
      <StyledInput
        onClick={openSelectMenu}
        ref={selectRef}
        type="text"
        id="multiselect"
        className="multiselect"
        placeholder="Select...."
        value={selectValue.map((option) => option.text).join(", ")}
        readOnly
        disabled={disabled}
      />
      <StyledCloseMenu onClick={() => setSelectValue([])}><FontAwesomeIcon icon={faTimes} /></StyledCloseMenu>
      {options && isOpen && (
        <StyledMenu className="multiselect">
          {options.map((option) => (
            <StyleMenuItem
              key={option.value}
              onClick={(event) => handleSelect(option, event)}
            >
              {option.text}
            </StyleMenuItem>
          ))}
        </StyledMenu>
      )}
    </div>
  );
}

export default memo(MultiSelect);
