import styled from "@emotion/styled/macro";
import { BACKGROUND_HOVER, TEXT_COLOR } from "../../constants";

export const StyledLabel = styled.label`
  display: block;
  margin-bottom: 6px;
  color: ${TEXT_COLOR};
  font-size: 1.2rem;
`;

export const StyledInput = styled.input<{disabled: boolean}>`
  padding: 10px;
  border-radius: 4px;
  width: 20rem;
  font-size: 1rem;
  border: 0.8px solid ${TEXT_COLOR};
  cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'}
`;

export const StyleMenuItem = styled.li`
  padding: 8px;
  font-size: 1.3rem;
  margin: 4px 0;
  &:hover {
    background-color: ${BACKGROUND_HOVER}
  }
`;

export const StyledMenu = styled.ul`
  list-style: none;
  width: 12rem;
  padding: 12px;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  border-radius: 4px;
`;

export const StyledCloseMenu = styled.span`
  position: relative;
  right: 31px;
  cursor: pointer;
  color: ${TEXT_COLOR};
  padding: 8px;
`;
