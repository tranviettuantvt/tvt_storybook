import MultiSelect from "./MultiSelect";

export default {
  title: "Atomic/MultiSelect",
  component: MultiSelect,
  tags: ["autodocs"],
};

export const Default = {
  args: {
    disabled:true,
    options: [
      {
        value: 0,
        text: "Angular",
      },
      {
        value: 1,
        text: "Bootstrap",
      },
      {
        value: 2,
        text: "React.js",
      },
      {
        value: 3,
        text: "Vue.js",
      },
    ],
  },
};
