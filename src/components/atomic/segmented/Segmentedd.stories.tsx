import { Avatar } from "antd";
import Segmentedd from "./Segmentedd";
import { UserOutlined } from "@ant-design/icons";

export default {
  title: "atomic/Segmented",
  component: Segmentedd,
  tags: ["autodocs"],
  argTypes: {
    disabled: { control: "boolean" },
    direction: { control: "radio" },
  },
};

export const Default = {
  args: {
    disabled: false,
    direction: "vertical",
    options: ["Map", "Transit", "Satellite"],
  },
};

export const Custom = {
  args: {
    options: [
      {
        label: (
          <div style={{ padding: 4 }}>
            <Avatar src="https://xsgames.co/randomusers/avatar.php?g=pixel" />
            <div>User 1</div>
          </div>
        ),
        value: "user1",
      },
      {
        label: (
          <div style={{ padding: 4 }}>
            <Avatar style={{ backgroundColor: "#f56a00" }}>K</Avatar>
            <div>User 2</div>
          </div>
        ),
        value: "user2",
      },
      {
        label: (
          <div style={{ padding: 4 }}>
            <Avatar
              style={{ backgroundColor: "#87d068" }}
              icon={<UserOutlined />}
            />
            <div>User 3</div>
          </div>
        ),
        value: "user3",
      },
    ],
  },
};
