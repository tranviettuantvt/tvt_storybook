import React, { memo } from "react";
import { Segmented, Space } from "antd";

type SegmentedItem = {
  label: React.ReactNode;
  value: string;
  disabled?: boolean;
};
type SegmenteddProps = {
  disabled?: boolean;
  direction?: "vertical" | "horizontal";
  options: string[] | SegmentedItem[];
};
const Segmentedd: React.FC<SegmenteddProps> = ({
  disabled = false,
  direction = "vertical",
  options,
}) => (
  <Space direction={direction}>
    <Segmented options={options} disabled={disabled} />
  </Space>
);

export default memo(Segmentedd);
