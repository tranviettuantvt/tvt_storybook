import Anchor from "./Anchor";

export default {
  title: "atomic/Anchor",
  component: Anchor,
  tags: ["autodocs"],
  argTypes: {
    direction: { control: "radio" },
  },
};

export const Default = {
  args: {
    direction: "horizontal",
    items: [
      {
        key: "part-1",
        href: "#part-1",
        title: "Part 1",
      },
      {
        key: "part-2",
        href: "#part-2",
        title: "Part 2",
      },
      {
        key: "part-3",
        href: "#part-3",
        title: "Part 3",
      },
    ],
    contents: [
      {
        id: "part-1",
        content: "Captain America",
        b_color:"rgba(0,255,0,0.02)"
      },
      {
        id: "part-2",
        content: "Iron Man",
        b_color: "rgba(0,0,255,0.02)"
      },
      {
        id: "part-3",
        content: "Super Man",
        b_color: "#FFFBE9"
      },
    ],
  },
};
