import styled from "@emotion/styled";

export const StyledAnchor = styled.div<{ direction: "vertical" | "horizontal" }>`
    position: fixed;
  display: flex;
  flex-direction: ${(props) => (props.direction === "vertical" ? "column" : "row")};
  justify-content: ${(props) => (props.direction === "vertical" ? "flex-start" : "center")};
  align-items: ${(props) => (props.direction === "vertical" ? "flex-start" : "center")};
`;

export const StyledAnchorItem = styled.a`
  margin: 8px;
  color: #1890ff;
  text-decoration: none;
  transition: color 0.3s;

  &:hover {
    color: #40a9ff;
  }
`;
