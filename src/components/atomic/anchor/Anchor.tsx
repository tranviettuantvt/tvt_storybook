import React, { memo, useState } from "react";
import { StyledAnchor, StyledAnchorItem } from "./styles";

type AnchorItem = {
  key: string;
  href: string;
  title: string;
};

type AnchorContent = {
  id: string;
  content: React.ReactNode;
  b_color: string;
};

type AnchorProps = {
  direction?: "vertical" | "horizontal";
  items: AnchorItem[];
  contents: AnchorContent[];
};

function Anchor({ direction = "vertical", items, contents }: AnchorProps) {
  const [activeContentId, setActiveContentId] = useState(contents[0].id);

  const handleItemClick = (event: React.MouseEvent<HTMLAnchorElement>) => {
    event.preventDefault();
    const clickedItemId = event.currentTarget.hash.slice(1);
    setActiveContentId(clickedItemId);
  };

  return (
    <>
      <StyledAnchor direction={direction}>
        {items.map((item) => (
          <StyledAnchorItem
            key={item.key}
            href={`#${item.key}`}
            onClick={handleItemClick}
          >
            {item.title}
          </StyledAnchorItem>
        ))}
      </StyledAnchor>
      {contents.map((content) => (
        <div
          key={content.id}
          id={content.id}
          style={{
            display: activeContentId === content.id ? "block" : "none",
            width: "100vw",
            height: "100vh",
            textAlign: "center",
            background: content.b_color,
          }}
        >
          {content.content}
        </div>
      ))}
    </>
  );
}

export default memo(Anchor);
