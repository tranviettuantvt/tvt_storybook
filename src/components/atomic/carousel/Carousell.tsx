import React, { memo } from "react";
import { Carousel } from "antd";

const contentStyle: React.CSSProperties = {
  margin: 0,
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
type CarouselProps = {
  autoplay?: boolean;
  effect: "scrollx" | "fade";
  children: React.ReactNode;
};
const Carousell: React.FC<CarouselProps> = ({ autoplay=false, effect, children }) => {
  const onChange = (currentSlide: number) => {
    console.log(currentSlide);
  };

  return (
    <Carousel autoplay={autoplay} effect={effect} afterChange={onChange}>
      {children}
    </Carousel>
  );
};

export default memo(Carousell);
