import Carousell from "./Carousell";

export default {
  title: "atomic/Carousel",
  component: Carousell,
  tags: ["autodocs"],
  argTypes: {
    autoplay: { control: "boolean" },
    effect: { options: ["scrollx", "fade"], control: { type: "radio" } },
  },
};
const contentStyle: React.CSSProperties = {
  margin: 0,
  height: "260px",
  color: "#fff",
  lineHeight: "260px",
  textAlign: "center",
  background: "#364d79",
};
export const Default = () => (
  <>
    <Carousell autoplay={false} effect="fade">
      <div>
        <h3 style={contentStyle}>Captain America</h3>
      </div>
      <div>
        <h3 style={contentStyle}>Captain America</h3>
      </div>
      <div>
        <h3 style={contentStyle}>Captain America</h3>
      </div>
      <div>
        <h3 style={contentStyle}>Captain America</h3>
      </div>
    </Carousell>
  </>
);
