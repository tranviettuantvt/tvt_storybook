import SingleSelect from "./SingleSelect";

export default {
  title: "atomic/SingleSelect",
  component: SingleSelect,
  tags: ["autodocs"],
};

export const Default = {
  args: {
    disabled: false,
    options: [
      {
        value: 0,
        text: "Angular",
      },
      {
        value: 1,
        text: "Bootstrap",
      },
      {
        value: 2,
        text: "React.js",
      },
      {
        value: 3,
        text: "Vue.js",
      },
    ],
  },
};
