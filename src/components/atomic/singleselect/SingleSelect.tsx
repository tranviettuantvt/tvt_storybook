import styled from "@emotion/styled";
import { memo } from "react";
import { TEXT_COLOR, BACKGROUND_HOVER } from "../../constants";

const StyledSingleSelect = styled.select`
  padding: 8px;
  font-size: 1.2rem;
  border-radius: 4px;
  border-color: ${TEXT_COLOR};

  option:hover {
    background-color: ${BACKGROUND_HOVER};
    cursor: pointer;
  }
`;

type SelectItem = {
  value: number;
  text: string;
};

type SingleSelectProps = {
  disabled?: boolean;
  options: SelectItem[];
};

const SingleSelect: React.FC<SingleSelectProps> = ({
  disabled = false,
  options,
}) => {
  return (
    <StyledSingleSelect disabled={disabled} >
      {options.map((option) => (
        <option key={option.value} value={option.text}>
          {option.text}
        </option>
      ))}
    </StyledSingleSelect>
  );
};

export default memo(SingleSelect);
