import styled from "@emotion/styled";

type StyledTypographyProps ={
  type?: 'success' | 'error' | 'text';
  level?: 'title' | 'heading' | 'text';
  decoration?: string;
  backgroundColor?: string;
  content?: string;
}

export const StyledTypography = styled('span')<StyledTypographyProps>`
  display: block;
  background-color: ${(props) => props.backgroundColor};

  font-size: ${(props) => {
    switch (props.level) {
      case "title":
        return "32px";
      case "heading":
        return "25px";
      case "text":
        return "16px";
      default:
        return "16px";
    }
  }};

  color: ${(props) => {
    switch (props.type) {
      case "success":
        return "green";
      case "error":
        return "red";
      case "text":
      default:
        return props.type;
    }
  }};

  font-weight: ${(props) => {
    switch (props.decoration) {
      case "bold":
        return "bold";
      default:
        return "normal";
    }
  }};

  font-style: ${(props) => {
    switch (props.decoration) {
      case "italic":
        return "italic";
      default:
        return "normal";
    }
  }};

  text-decoration: ${(props) => {
    switch (props.decoration) {
      case "underline":
        return "underline";
      default:
        return "none";
    }
  }};
`;