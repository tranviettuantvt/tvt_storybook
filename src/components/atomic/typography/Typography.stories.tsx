import Typography from "./Typography";

export default {
  title: "atomic/Typography",
  component: Typography,
  tags: ["autodocs"],
  argTypes: {
    backgroundColor: { control: "color" },
    type: { control: { type: 'color', presetColors:['success', 'error', 'text']} }
  },
};

export const Introduction={
    args: {
        type:'text',
        backgroundColor: null,
        level: 'title',
        decoration: null,
        content:'hello, I am Tuan'
    }
}

export const Error={
    args: {
        type:'error',
        backgroundColor: null,
        level: 'heading',
        decoration: 'bold',
        content:'Restricted Area'
    }
}
