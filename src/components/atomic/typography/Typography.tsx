import React, { memo } from "react";
import PropTypes from "prop-types";
import { StyledTypography } from "./styles";

type TypographyProps={
  type?: 'success' | 'error' | 'text';
  level?: 'title' | 'heading' | 'text';
  decoration?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
}
function Typography(props: TypographyProps) {
  const { type, level, decoration, backgroundColor, children }=props
  return (
    <StyledTypography
      as="span"
      type={type}
      level={level}
      decoration={decoration}
      backgroundColor={backgroundColor}
    >
      {children}
    </StyledTypography>
  );
}

export default memo(Typography);

Typography.propTypes = {
  type: PropTypes.oneOf(["success", "error", "text"]),
  backgroundColor: PropTypes.string,
  level: PropTypes.oneOf(["title", "heading", "text"]),
  decoration: PropTypes.oneOf(["bold", "italic", "underline"]),
  content: PropTypes.node,
};

Typography.defaultProps = {
  type: "success",
  backgroundColor: null,
  level: "title",
  decoration: null,
  children: "hello, I am Tuan",
};
