import React, { memo } from 'react';
import { Button, notification, Space } from 'antd';

type NotificationnProps = {
  type: 'success' | 'info' | 'warning' | 'error';
  message: string;
  descript: React.ReactNode;
};

const Notificationn: React.FC<NotificationnProps> = ({ type, message, descript }) => {
  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (t: 'success' | 'info' | 'warning' | 'error', m: string, d: React.ReactNode) => {
    api[t]({
      message: m,
      description: d
    });
  };

  return (
    <>
      {contextHolder}
      <Space>
        <Button onClick={() => openNotificationWithIcon(type, message, descript)}>{type}</Button>
      </Space>
    </>
  );
};

export default memo(Notificationn);
