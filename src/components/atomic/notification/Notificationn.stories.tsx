import Notificationn from "./Notificationn";

export default {
  title: "atomic/Notification",
  component: Notificationn,
  tags: ["autodocs"],
  argTypes: {
    type: { control: "radio" },
  },
};

export const Success = {
  args: {
    type: "success",
    message: "Notification Title",
    descript: <h3>You are success</h3>,
  },
};

export const Error = {
  args: {
    type: "error",
    message: "Notification Title",
    descript: <h3>You are fail</h3>,
  },
};
