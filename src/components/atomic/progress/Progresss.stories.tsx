import Progresss from "./Progresss";

export default {
    title: 'atomic/Progress',
    component: Progresss,
    tags: ["autodocs"],
    argTypes: {
        type: { control: "radio" },
        status: { control: "radio" },
    }
}

export const Circle={
    args: {
        type: 'circle',
        status:'success',
        percent: 90
    }
}

export const Dashboard={
    args: {
        type: 'dashboard',
        status:'active',
        percent: 70
    }
}