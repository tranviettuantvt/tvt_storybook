import React, { memo } from "react";
import { Progress } from "antd";

type ProgresssProps = {
  type: "line" | "circle" | "dashboard";
  status: "success" | "exception" | "normal" | "active";
  percent:number
};
const Progresss: React.FC<ProgresssProps> = ({type, status, percent}) => (
  <>
    <Progress percent={percent} status={status} type={type} />
  </>
);

export default memo(Progresss);
