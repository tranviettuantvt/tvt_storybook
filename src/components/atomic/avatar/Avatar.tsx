import React, { memo } from "react";
import PropTypes from "prop-types";
import { StyledImage } from "./styles";

type AvatarProps = {
  alt?: string;
  shape?: "circle" | "square"; // Define the shape property with the allowed values
  size?: "small" | "medium" | "large";
  src: string;
};

function Avatar(props: AvatarProps) {
  const { alt, shape, size, src } = props;
  return (
    <StyledImage
      as="img" // Render as img element
      className={`avatar-${size}`}
      alt={alt}
      src={src}
      shape={shape}
    />
  );
}

export default memo(Avatar);


Avatar.propTypes = {
  src: PropTypes.string,
  shape: PropTypes.oneOf(["square", "circle"]),
  alt: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium", "large"]),
};

Avatar.defaultProps = {
  src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU",
  alt: "I am dog",
  size: "medium",
  shape: "circle",
};
