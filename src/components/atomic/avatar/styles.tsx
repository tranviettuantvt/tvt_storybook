import styled from "@emotion/styled";

type StyledImageProps = {
  shape?: "circle" | "square";
};

export const StyledImage = styled("img")<StyledImageProps>`
  object-fit: cover;
  border-radius: ${(props) => (props.shape === "circle" ? "50%" : "0")};
  &.avatar-large {
    width: 300px;
    height: 300px;
  }
  &.avatar-medium {
    width: 200px;
    height: 200px;
  }
  &.avatar-small {
    width: 100px;
    height: 100px;
  }
`;
