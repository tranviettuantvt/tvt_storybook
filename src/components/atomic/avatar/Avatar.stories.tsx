import Avatar from "./Avatar";

export default {
    title:'Atomic/Avatar',
    component: Avatar,
    tags: ["autodocs"],
    argTypes: {
        shape: {control: 'radio'}
    }
}

export const Dog={
    args: {
        src:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRga28YpBbOzEHk-CnVLCXn-cB5jsHzIPkgoQ&usqp=CAU',
        alt:'I am dog',
        size:'medium',
        shape:'circle'
    }
}

export const Cat={
    args: {
        src:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM1cDnT1Q5ZrkfLfxiSgFvC2ZsjpngynJGvg&usqp=CAU',
        alt:'I am Cat',
        size:'large',
        shape:'square'
    }
}