import Dropdown from "./Dropdown";

export default {
  title: "atomic/Dropdown",
  component: Dropdown,
  tags: ["autodocs"],
  argTypes: {
    action: {
      control: "radio",
    },
  },
};

export const Default = {
  args: {
    icon: "۞",
    text:"Hover me",
    action: "hover",
    items: [
      {
        key: "1",
        label: "Option 1",
      },
      {
        key: "2",
        label: "Option 2",
      },
      {
        key: "3",
        label: "Option 3",
      },
    ],
  },
};

export const Custom = {
  args: {
    text:'Click me',
    action: "click",
    items: [
      {
        key: "1",
        label: (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.aliyun.com"
          >
            2nd menu item
          </a>
        ),
      },
      {
        key: "2",
        label: <h5 style={{ margin: "0" }}>2nd menu item </h5>,
      },
    ],
  },
};
