import React, { memo, useState } from "react";
import {
  StyledDropdownButton,
  StyledDropdownMenu,
  StyledDropdownMenuItem,
} from "./styles";

type DropdownItem = {
  key: string;
  label: React.ReactNode;
};

type DropdownProps = {
  icon?: string;
  text: string;
  items?: DropdownItem[] | undefined;
  action: "click" | "hover";
};

function Dropdown({ icon, text, items, action = "hover" }: DropdownProps) {
  const [isOpen, setIsOpen] = useState(false);

  const openDropdown = () => {
    setIsOpen(true);
  };
  const hideDropdown = () => {
    setIsOpen(false);
  };

  const handleMenuItemClick = (key: string) => {
    console.log("Clicked menu item:", key);
    setIsOpen(false);
  };
  
  return (
    <StyledDropdownButton
      onMouseEnter={action === "hover" ? openDropdown : undefined}
      onClick={action === "click" ? openDropdown : undefined}
      onMouseLeave={hideDropdown}
    >
      <span style={{fontSize:"1.4rem"}}>{icon}</span>
      <span>{text}</span>
      {items && isOpen && (
        <StyledDropdownMenu
          onMouseEnter={openDropdown}
          // onMouseLeave={hideDropdown}
        >
          {items.map((item) => (
            <StyledDropdownMenuItem
              key={item.key}
              onClick={() => handleMenuItemClick(item.key)}
            >
              {item.label}
            </StyledDropdownMenuItem>
          ))}
        </StyledDropdownMenu>
      )}
    </StyledDropdownButton>
  );
}

export default memo(Dropdown);
