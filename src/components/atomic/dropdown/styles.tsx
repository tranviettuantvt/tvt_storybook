import { keyframes } from "@emotion/react";
import styled from "@emotion/styled/macro";
const fadeIn = keyframes`
from {
    opacity: 0;
    transform: translate3d(0, -10px, 0);
  }
  to {
    opacity: 1;
    transform: translate3d(0, 0, 0);
  }
`;


export const StyledDropdownButton = styled.button`
  display: flex;
  gap: 8px;
  align-items: center;
  position: relative;
  padding: 8px 16px 8px 4px;
  background-color: transparent;
  border: none;
  color: inherit;
  font-family: inherit;
  font-size: 1.4rem;
  cursor: pointer;
`;

export const StyledDropdownMenu = styled.ul`
  position: absolute;
  top: 100%;
  left: 0;
  width: 180px;
  padding: 0;
  margin: 0;
  background-color: #fff;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
  list-style-type: none;
  display: flex;
  flex-direction: column;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.3s ease-in-out, visibility 0s ease-in-out 0.3s, transform 0.3s ease-in-out;
  
  ${StyledDropdownButton}:hover & {
    border-top: 2px solid black;
    opacity: 1;
    visibility: visible;
    animation: ${fadeIn} ease-in .2s;
  }
`;

export const StyledDropdownMenuItem = styled.li`
  text-align: start;
  padding: 16px 12px;
  cursor: pointer;
  &:hover {
    background-color: #f5f5f5;
  }
`;
