import React from "react";
import { useNavigate } from "react-router-dom";

function DashBoard() {
  const navigate = useNavigate();
  const storedData = JSON.parse(localStorage.getItem("userData"));
  const handleLogout = () => {
    localStorage.removeItem("userData");
    navigate("/login");
  };
  return (
    <div>
      <nav class="navbar">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/" class="nav-link">
              Home
            </a>
          </li>
          <li class="nav-item">
            <a href="/contact" class="nav-link">
              Contact
            </a>
          </li>
        </ul>
        <div class="user-info">
          Hello {storedData?.email}
          <span class="logout" onClick={() => handleLogout()}>
            Logout
          </span>
        </div>
      </nav>

      <div class="sidebar">
        <ul class="sidebar-menu">
          <li class="sidebar-menu-item">
            <a href="/" class="sidebar-menu-link">
              Button
            </a>
          </li>
          <li class="sidebar-menu-item">
            <a href="/about" class="sidebar-menu-link">
              Alert
            </a>
          </li>
          <li class="sidebar-menu-item">
            <a href="/services" class="sidebar-menu-link">
              Card
            </a>
          </li>
          <li class="sidebar-menu-item">
            <a href="/contact" class="sidebar-menu-link">
              Form
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default DashBoard;
