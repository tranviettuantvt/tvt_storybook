import { Alert, Button, TextField, Typography } from "@mui/material";
import { Box, Container } from "@mui/system";
import React, { useState } from "react";
import User from "../data/user.json";
import { useNavigate } from "react-router-dom";

function Login() {
  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState({});
  const [submit, setSubmit] = useState(false);
  const navigate = useNavigate();

  const validateForm = (e) => {
    var mailformat =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!e.match(mailformat)) {
      setError({ ...error, email: "Email is in wrong format" });
    } else {
      setError({});
    }
  };

  const handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setSubmit(false);
    if (name === "email") {
      validateForm(value);
      setUser({ ...user, email: value });
    } else if (name === "password") setUser({ ...user, password: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = user;
    console.log(email, password, "hello");
    if (email === "" || password === "") {
      setError({ email: "fill the email", pass: "fill the password" });
      console.log(error);
      return;
    }
    const useracc = User.find((useracc) => useracc.email === email);
    if (useracc) {
      if (useracc.password !== password) {
        setSubmit(false);
        setError({ ...error, pass: "Wrong password" });
      } else {
        setSubmit(true);
        setError({});
        setUser({
          email: "",
          password: "",
        });
      }
      const userData = { email, password, token: "1" };
      localStorage.setItem("userData", JSON.stringify(userData));
      navigate("/");
    } else {
      setSubmit(false);
      setError({ ...error, email: "Not found email" });
    }
  };
  return (
    <Container component="main">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box
          component="form"
          onSubmit={handleSubmit}
          sx={{ mt: 1, width: "532px " }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={user.email}
            onChange={handleInputChange}
          />
          {error.email && <Alert severity="error">{error.email}</Alert>}
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={user.password}
            onChange={handleInputChange}
          />
          {error.pass && <Alert severity="error">{error.pass}</Alert>}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          {submit && <Alert severity="success">Success Login</Alert>}
        </Box>
      </Box>
    </Container>
  );
}

export default Login;
