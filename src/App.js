import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import Login from "./components/Login";
import DashBoard from "./components/DashBoard";
import FormikLogin from "./components/FormikLogin";
import "./src/fontawasome.js"
function App() {
  const PrivateRoute = ({ children }) => {
    const storedData = JSON.parse(localStorage.getItem("userData"));
    return storedData?.token ? <>{children}</> : <Navigate to="/login" />;
  };
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<FormikLogin />} />
        <Route
          index
          path="/"
          element={
            <PrivateRoute>
              <DashBoard />
            </PrivateRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
